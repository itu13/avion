CREATE SEQUENCE seqadministrator START WITH 100;

CREATE SEQUENCE seqassurance START WITH 100;

CREATE SEQUENCE seqassuranceavion START WITH 100;

CREATE SEQUENCE seqavion START WITH 100;

CREATE SEQUENCE seqkilometrage START WITH 100;

CREATE SEQUENCE seqmaintenance START WITH 100;

CREATE SEQUENCE seqtoken START WITH 100;

CREATE SEQUENCE seqtypemaintenance START WITH 100;

CREATE  TABLE administrator ( 
	id                   varchar(255)  DEFAULT nextval('seqadministrator'::regclass) NOT NULL ,
	firstname            varchar(100)  NOT NULL ,
	lastname             varchar(100)  NOT NULL ,
	email                varchar(50)  NOT NULL ,
	"password"           varchar(50)  NOT NULL ,
	birthdate            date  NOT NULL ,
	creationdate         date DEFAULT CURRENT_DATE NOT NULL ,
	CONSTRAINT pk_administrateur_id PRIMARY KEY ( id )
 );

CREATE  TABLE assurance ( 
	id                   varchar(20)  DEFAULT nextval('seqassurance'::regclass) NOT NULL ,
	validite             varchar(50)  NOT NULL ,
	montant              float8  NOT NULL ,
	CONSTRAINT pk_assurance_id PRIMARY KEY ( id )
 );

CREATE  TABLE brand ( 
	id                   varchar(20)  NOT NULL ,
	brandname            varchar(100)  NOT NULL ,
	administrator_id     varchar(20)  NOT NULL ,
	CONSTRAINT pk_brand_id PRIMARY KEY ( id ),
	CONSTRAINT brand_brandname_key UNIQUE ( brandname  ) 
 );

CREATE  TABLE token ( 
	id                   varchar(20)  DEFAULT nextval('seqtoken'::regclass) NOT NULL ,
	administrator_id     varchar(20)  NOT NULL ,
	token_value          varchar(255)  NOT NULL ,
	expiration_date      timestamp  NOT NULL ,
	creation_date        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	CONSTRAINT pk_token_id PRIMARY KEY ( id ),
	CONSTRAINT token_token_value_key UNIQUE ( token_value  ) 
 );

CREATE  TABLE typemaintenance ( 
	id                   varchar(20)  DEFAULT nextval('seqtypemaintenance'::regclass) NOT NULL ,
	name                 varchar(100)  NOT NULL ,
	CONSTRAINT pk_typemaintenance_id PRIMARY KEY ( id )
 );

CREATE  TABLE avion ( 
	id                   varchar(20)  DEFAULT nextval('seqavion'::regclass) NOT NULL ,
	brand_id             varchar(20)  NOT NULL ,
	planenumber          varchar(10)  NOT NULL ,
	creationdate         date DEFAULT CURRENT_DATE NOT NULL ,
	administrator_id     varchar(20)  NOT NULL ,
	photo                text   ,
	CONSTRAINT pk_avion_id PRIMARY KEY ( id ),
	CONSTRAINT avion_planenumber_key UNIQUE ( planenumber  ) 
 );

CREATE  TABLE kilometrage ( 
	id                   varchar(20)  DEFAULT nextval('seqkilometrage'::regclass) NOT NULL ,
	avion_id             varchar(20)  NOT NULL ,
	starting_km          float8  NOT NULL ,
	ending_km            float8  NOT NULL ,
	checkin_date         date DEFAULT CURRENT_DATE NOT NULL ,
	administrator_id     varchar(20)  NOT NULL ,
	CONSTRAINT pk_kilometrage_id PRIMARY KEY ( id )
 );

CREATE  TABLE maintenance ( 
	id                   varchar(20)  DEFAULT nextval('seqmaintenance'::regclass) NOT NULL ,
	avion_id             varchar(20)  NOT NULL ,
	cost                 float8  NOT NULL ,
	maintenance_date     date DEFAULT CURRENT_DATE NOT NULL ,
	typemaintenance      varchar(20)  NOT NULL ,
	CONSTRAINT pk_maintenance_id PRIMARY KEY ( id )
 );

CREATE  TABLE assuranceavion ( 
	id                   varchar(20)  DEFAULT nextval('seqassuranceavion'::regclass) NOT NULL ,
	idavion              varchar(20)  NOT NULL ,
	idassurance          varchar(20)  NOT NULL ,
	quantite             integer  NOT NULL ,
	dateenregistrement   date DEFAULT CURRENT_DATE NOT NULL ,
	dateexpiration       date DEFAULT CURRENT_DATE NOT NULL ,
	CONSTRAINT pk_assuranceavion_id PRIMARY KEY ( id )
 );



ALTER TABLE assuranceavion ADD CONSTRAINT fk_assuranceavion_assurance FOREIGN KEY ( idassurance ) REFERENCES assurance( id );

ALTER TABLE assuranceavion ADD CONSTRAINT fk_assuranceavion_avion FOREIGN KEY ( idavion ) REFERENCES avion( id );

ALTER TABLE avion ADD CONSTRAINT fk_avion_administrator FOREIGN KEY ( administrator_id ) REFERENCES administrator( id );

ALTER TABLE avion ADD CONSTRAINT fk_avion_brand FOREIGN KEY ( brand_id ) REFERENCES brand( id );

ALTER TABLE brand ADD CONSTRAINT fk_brand_administrator FOREIGN KEY ( administrator_id ) REFERENCES administrator( id );

ALTER TABLE kilometrage ADD CONSTRAINT fk_kilometrage_administrator FOREIGN KEY ( administrator_id ) REFERENCES administrator( id );

ALTER TABLE kilometrage ADD CONSTRAINT fk_kilometrage_avion FOREIGN KEY ( avion_id ) REFERENCES avion( id );

ALTER TABLE maintenance ADD CONSTRAINT fk_maintenance_avion FOREIGN KEY ( avion_id ) REFERENCES avion( id );

ALTER TABLE maintenance ADD CONSTRAINT fk_maintenance_typemaintenance FOREIGN KEY ( typemaintenance ) REFERENCES typemaintenance( id );

ALTER TABLE token ADD CONSTRAINT fk_token_administrator FOREIGN KEY ( administrator_id ) REFERENCES administrator( id );

CREATE OR REPLACE VIEW v_assurance as SELECT avion.id,
    avion.brand_id,
    avion.planenumber,
    avion.creationdate,
    avion.administrator_id,
    (( SELECT ((date_part('year'::text, f.f) * (12)::double precision) + date_part('month'::text, f.f))
           FROM age((a.dateexpiration)::timestamp with time zone, ((now())::date)::timestamp with time zone) f(f)))::integer AS monthdiff
   FROM ((avion
     JOIN assuranceavion a ON (((avion.id)::text = (a.idavion)::text)))
     JOIN assurance a2 ON (((a.idassurance)::text = (a2.id)::text)));;

CREATE OR REPLACE VIEW v_validtoken as SELECT token.id,
    token.administrator_id,
    token.token_value,
    token.expiration_date,
    token.creation_date
   FROM token
  WHERE (EXTRACT(epoch FROM ((token.expiration_date)::timestamp with time zone - now())) > (0)::numeric);;

INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '75317', 'Abel', 'Spencer', 'oupo@j--dw-.org', 'XufH48Uh5lS', '2007-04-22', '2021-06-18' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '26902', 'Erick', 'Leonard', 'omuk@---dio.com', 'SYV2IT3GUT785Bj', '2001-10-07', '2020-09-27' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '33600', 'Janice', 'Montes', 'ljlg1@---ln-.com', '4X5OowFi1R85HUI', '2000-11-18', '2021-02-27' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '52309', 'Gretchen', 'Proctor', 'vxxd.xgfbkp@-e--ch.org', 'SP6bLNQ6wN4s8l1', '2006-02-12', '2020-06-01' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '68950', 'Lawanda', 'Velazquez', 'ppkdi@i--jvj.net', 'QKL7bV45IQS3276', '2004-11-20', '2021-07-22' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '47952', 'Robbie', 'Wilkins', 'vlpj@----vg.com', 'UbLaGb1uDY6', '2009-06-13', '2021-07-12' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '36132', 'Carla', 'Randall', 'ddth233@-p----.com', 'QtXSiG8DUn', '2004-08-14', '2020-03-25' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '66758', 'Heath', 'Dickson', 'rucc@b--c-i.com', 'RiE8TSQRilK3Xrh', '2008-04-06', '2020-05-26' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '75032', 'Kendra', 'Rodgers', 'cuoa.tbwkl@cctw--.net', 'NTpXT4XE509R75', '2004-06-26', '2020-09-04' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '14296', 'Brandie', 'Finley', 'ckzp@sztm-k.org', 'FBW172UCYpjuXL', '2004-04-10', '2021-04-12' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '2', 'teddy', 'bear', 'jean@gmail.com', 'jean1234', '2002-05-14', '2022-01-01' ); 
INSERT INTO administrator( id, firstname, lastname, email, password, birthdate, creationdate ) VALUES ( '3', 'teddy', 'bear', 'jean@gmail.com', 'jean1234', '2002-05-14', '2022-01-01' ); 
INSERT INTO assurance( id, validite, montant ) VALUES ( '1', '1', 50000.0 ); 
INSERT INTO assurance( id, validite, montant ) VALUES ( '2', '3', 140000.0 ); 
INSERT INTO assurance( id, validite, montant ) VALUES ( '3', '12', 600000.0 ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '75317', 'Rebanicar International Inc', '66758' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '26902', 'Unpebopor Holdings Company', '26902' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '33600', 'Cipweradicator Holdings Company', '68950' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '52309', 'Uptumopicator WorldWide Group', '68950' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '68950', 'Surtanamar WorldWide Inc', '33600' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '47952', 'Hapzapopicator WorldWide Company', '75317' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '36132', 'Surjubupax WorldWide Corp.', '52309' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '66758', 'Emvenupover WorldWide Group', '36132' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '75032', 'Qwimunicor Direct Group', '14296' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '14296', 'Cipnipamar Holdings Inc', '66758' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '04766', 'Endhupupar Holdings ', '75317' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '08341', 'Endrobackar Direct Group', '26902' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '28363', 'Inhupazz Holdings ', '14296' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '14655', 'Inbananex WorldWide Corp.', '26902' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '37010', 'Lomzapamistor WorldWide ', '14296' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '43663', 'Lomkilupan Direct Group', '47952' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '77227', 'Gronipazz Holdings Group', '14296' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '88763', 'Rerobupower WorldWide ', '14296' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '38751', 'Windimedower Direct Group', '14296' ); 
INSERT INTO brand( id, brandname, administrator_id ) VALUES ( '93242', 'Dopcadopor Holdings Group', '68950' ); 
INSERT INTO token( id, administrator_id, token_value, expiration_date, creation_date ) VALUES ( '11', '2', 'amVhbkBnbWFpbC5jb21qZWFuMTIzNDE2Njg0MjkxOTc3Mjc=', '2022-11-13 15:33:17.727', '2022-11-14 15:33:17.727' ); 
INSERT INTO token( id, administrator_id, token_value, expiration_date, creation_date ) VALUES ( '2', '2', 'amVhbkBnbWFpbC5jb21qZWFuMTIzNDE2Njg3NTYyODAzNzE=', '2022-11-19 10:24:40.371', '2022-11-18 10:24:40.371' ); 
INSERT INTO token( id, administrator_id, token_value, expiration_date, creation_date ) VALUES ( '3', '2', 'amVhbkBnbWFpbC5jb21qZWFuMTIzNDE2NjkwNDU2NDY0MTE=', '2022-11-22 18:47:26.411', '2022-11-21 18:47:26.411' ); 
INSERT INTO token( id, administrator_id, token_value, expiration_date, creation_date ) VALUES ( '4', '2', 'amVhbkBnbWFpbC5jb21qZWFuMTIzNDE2NzA1NjU0NDk4NDQ=', '2022-12-10 08:57:29.845', '2022-12-09 08:57:29.845' ); 
INSERT INTO token( id, administrator_id, token_value, expiration_date, creation_date ) VALUES ( '5', '2', 'amVhbkBnbWFpbC5jb21qZWFuMTIzNDE2NzA2ODk2ODgwMTg=', '2022-12-11 19:28:08.019', '2022-12-10 19:28:08.019' ); 
INSERT INTO token( id, administrator_id, token_value, expiration_date, creation_date ) VALUES ( '6', '2', 'amVhbkBnbWFpbC5jb21qZWFuMTIzNDE2NzEwODA4MDk3MjE=', '2022-12-16 08:06:49.721', '2022-12-15 08:06:49.721' ); 
INSERT INTO typemaintenance( id, name ) VALUES ( '1', 'vidange' ); 
INSERT INTO typemaintenance( id, name ) VALUES ( '2', 'pneu' ); 
INSERT INTO typemaintenance( id, name ) VALUES ( '3', 'moteur' ); 
INSERT INTO typemaintenance( id, name ) VALUES ( '4', 'ailes' ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '75317', '37010', '5755VJC', '2016-01-28', '66758', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '26902', '47952', '4186OBD', '2019-12-20', '26902', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '33600', '38751', '4547KCX', '2020-03-18', '68950', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '52309', '75317', '7113OJJ', '2017-11-01', '68950', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '68950', '14296', '1143LGC', '2020-03-17', '33600', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '47952', '88763', '2243LNO', '2004-10-16', '75317', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '36132', '93242', '6338VXX', '2014-12-13', '52309', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '66758', '75317', '1782FBK', '2006-04-04', '36132', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '75032', '88763', '5362GHC', '2001-09-21', '14296', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '14296', '38751', '3391IPP', '2010-12-18', '66758', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '04766', '93242', '4130IBY', '2000-07-18', '75317', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '08341', '14296', '3837CMC', '2018-10-04', '26902', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '28363', '68950', '7463VQD', '2015-05-21', '14296', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '14655', '04766', '2772JJW', '2017-08-01', '26902', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '37010', '26902', '7117HFJ', '2017-09-16', '14296', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '43663', '77227', '3415ORH', '2005-11-22', '47952', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '77227', '37010', '4211LRU', '2003-04-30', '14296', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '88763', '68950', '1170LNC', '2012-08-22', '14296', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '38751', '38751', '2398OKC', '2020-05-04', '14296', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '93242', '08341', '7508TBW', '2009-04-01', '68950', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '35229', '75317', '4441CTW', '2016-10-21', '52309', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '77451', '33600', '7929UYC', '2015-10-11', '14296', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '35517', '14655', '4963SZT', '2000-01-28', '52309', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '23362', '43663', '4448HHW', '2011-09-26', '26902', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '38800', '88763', '1007FTW', '2016-03-14', '33600', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '66048', '66758', '6993DLY', '2007-12-06', '52309', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '63934', '26902', '2022XWC', '2021-01-14', '47952', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '70482', '47952', '1016SFG', '2009-12-24', '75032', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '65053', '75032', '4035MWA', '2018-02-13', '26902', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '77020', '38751', '2733XWR', '2002-11-05', '47952', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '65232', '66758', '3154OKN', '2004-02-25', '66758', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '44116', '04766', '1581JOQ', '2014-03-07', '47952', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '76877', '93242', '4572FEB', '2007-03-05', '36132', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '29880', '14655', '8356YKR', '2009-01-12', '75317', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '05236', '08341', '2715AXW', '2016-07-14', '26902', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '33769', '36132', '7148JJT', '2004-02-27', '66758', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '83886', '14296', '5560GFN', '2006-03-03', '52309', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '54122', '43663', '6340DOM', '2009-12-04', '36132', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '08158', '75317', '5047QQT', '2005-01-23', '26902', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '34759', '52309', '6693EQO', '2012-11-29', '75032', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '06234', '08341', '8147IJV', '2017-10-08', '36132', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '22718', '52309', '2786WLT', '2016-03-12', '68950', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '40732', '88763', '4755QJS', '2016-09-14', '75032', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '42581', '08341', '2985EBY', '2009-02-27', '75317', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '65919', '68950', '4508XLJ', '2017-03-11', '75317', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '78956', '68950', '0875ICW', '2010-04-04', '75032', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '73733', '52309', '5266YJS', '2019-08-25', '47952', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '90625', '14655', '8364KBZ', '2008-06-09', '75317', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '12182', '93242', '5542NGR', '2011-07-13', '66758', null ); 
INSERT INTO avion( id, brand_id, planenumber, creationdate, administrator_id, photo ) VALUES ( '74338', '28363', '5632VXT', '2017-03-13', '66758', null ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '75317', '83886', 731.057373046875, 2461.48828125, '2014-08-11', '66758' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '26902', '37010', 70.99203491210938, 2837.50927734375, '2003-07-14', '26902' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '33600', '78956', 67.12001037597656, 2837.295654296875, '2001-10-06', '68950' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '52309', '75317', 768.156982421875, 2359.434814453125, '2012-03-27', '68950' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '68950', '38800', 227.33465576171875, 1157.6971435546875, '2009-10-11', '33600' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '47952', '40732', 660.3196411132812, 1050.602294921875, '2018-11-25', '75317' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '36132', '74338', 806.673583984375, 2398.80419921875, '2009-03-27', '52309' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '66758', '26902', 29.817676544189453, 2610.069091796875, '2016-07-11', '36132' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '75032', '40732', 811.1701049804688, 1303.31298828125, '2008-12-21', '14296' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '14296', '73733', 945.4161987304688, 2849.237548828125, '2008-07-20', '66758' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '04766', '74338', 152.73361206054688, 2527.906982421875, '2013-12-14', '75317' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '08341', '35517', 910.2762451171875, 1007.758056640625, '2015-01-19', '26902' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '28363', '08341', 999.146240234375, 2473.10986328125, '2005-07-22', '14296' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '14655', '66048', 539.9926147460938, 2912.48388671875, '2006-11-30', '26902' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '37010', '52309', 977.512451171875, 2647.474609375, '2015-03-14', '14296' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '43663', '40732', 158.4378204345703, 1378.17724609375, '2016-10-12', '47952' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '77227', '54122', 549.0886840820312, 1652.8714599609375, '2004-05-20', '14296' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '88763', '28363', 93.54586791992188, 2503.34521484375, '2009-04-01', '14296' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '38751', '90625', 443.3903503417969, 1561.55859375, '2018-05-29', '14296' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '93242', '70482', 412.86187744140625, 1460.9976806640625, '2014-08-18', '68950' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '35229', '26902', 286.4975891113281, 2615.439453125, '2017-09-20', '52309' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '77451', '47952', 169.2379150390625, 2518.666015625, '2006-05-12', '14296' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '35517', '05236', 487.4576110839844, 2562.9716796875, '2004-06-09', '52309' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '23362', '34759', 943.1689453125, 2786.27587890625, '2006-06-30', '26902' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '38800', '40732', 890.6096801757812, 2048.5693359375, '2017-09-04', '33600' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '66048', '38751', 535.7579956054688, 1103.7882080078125, '2000-06-26', '52309' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '63934', '33600', 571.6705322265625, 1694.2830810546875, '2008-10-15', '47952' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '70482', '14655', 254.11520385742188, 1960.9482421875, '2006-05-02', '75032' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '65053', '35229', 690.7794189453125, 2813.25634765625, '2007-11-09', '26902' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '77020', '90625', 961.2112426757812, 2141.363525390625, '2013-03-09', '47952' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '65232', '88763', 333.78485107421875, 2115.90673828125, '2019-11-23', '66758' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '44116', '63934', 260.00714111328125, 2143.9423828125, '2013-04-19', '47952' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '76877', '74338', 419.37762451171875, 2795.3916015625, '2017-08-18', '36132' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '29880', '29880', 125.3974609375, 1429.8096923828125, '2003-12-31', '75317' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '05236', '65053', 747.8607788085938, 1647.368408203125, '2005-08-30', '26902' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '33769', '77227', 468.5741271972656, 1297.1148681640625, '2013-04-18', '66758' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '83886', '23362', 169.16259765625, 2362.00146484375, '2004-12-15', '52309' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '54122', '34759', 451.41387939453125, 2872.95458984375, '2001-03-15', '36132' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '08158', '33600', 283.0184020996094, 2123.987548828125, '2011-11-14', '26902' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '34759', '75032', 501.1109924316406, 1585.3853759765625, '2009-10-15', '75032' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '06234', '77020', 511.08709716796875, 2626.959716796875, '2010-03-28', '36132' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '22718', '75032', 498.3359680175781, 1671.4356689453125, '2009-12-18', '68950' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '40732', '40732', 335.2534484863281, 2444.0615234375, '2001-07-18', '75032' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '42581', '77020', 319.6490783691406, 2233.07080078125, '2011-01-26', '75317' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '65919', '08341', 842.7615356445312, 2433.894775390625, '2000-10-20', '75317' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '78956', '08341', 53.80073165893555, 1115.40869140625, '2006-10-01', '75032' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '73733', '66758', 899.5079345703125, 1901.2210693359375, '2013-08-29', '47952' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '90625', '29880', 732.1093139648438, 2066.5224609375, '2016-09-26', '75317' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '12182', '12182', 558.3031005859375, 2247.081298828125, '2009-01-05', '66758' ); 
INSERT INTO kilometrage( id, avion_id, starting_km, ending_km, checkin_date, administrator_id ) VALUES ( '74338', '44116', 218.6373291015625, 2378.086669921875, '2009-06-23', '66758' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '75317', '83886', 7311469.0, '2022-06-13', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '26902', '47952', 9014476.0, '2022-02-14', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '33600', '35229', 4968226.0, '2021-03-05', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '52309', '35229', 9858769.0, '2022-08-04', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '68950', '04766', 8571240.0, '2021-03-02', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '47952', '26902', 9874208.0, '2022-02-28', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '36132', '77227', 2281579.0, '2022-07-09', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '66758', '76877', 747938.0, '2022-02-23', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '75032', '12182', 7431577.0, '2021-06-23', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '14296', '33769', 9495833.0, '2022-09-06', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '04766', '75317', 305132.0, '2022-04-24', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '08341', '66758', 6916554.0, '2021-09-19', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '28363', '12182', 8516909.0, '2022-08-05', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '14655', '66758', 567471.0, '2021-03-01', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '37010', '73733', 4082851.0, '2021-02-04', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '43663', '70482', 3523888.0, '2021-10-31', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '77227', '90625', 9936866.0, '2022-08-08', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '88763', '78956', 5619453.0, '2021-04-19', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '38751', '73733', 4759845.0, '2022-11-11', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '93242', '38800', 494115.0, '2022-09-15', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '35229', '93242', 5822724.0, '2021-05-01', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '77451', '78956', 8512430.0, '2022-05-30', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '35517', '88763', 2286738.0, '2022-10-17', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '23362', '66758', 1531859.0, '2022-02-24', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '38800', '37010', 9639223.0, '2022-12-19', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '66048', '38751', 2326256.0, '2021-02-12', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '63934', '66048', 6166417.0, '2022-01-29', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '70482', '42581', 6088954.0, '2021-04-21', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '65053', '47952', 2476190.0, '2022-12-04', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '77020', '63934', 8393601.0, '2021-12-17', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '65232', '08158', 6889954.0, '2021-05-05', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '44116', '77020', 7976571.0, '2022-01-31', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '76877', '76877', 1237242.0, '2022-02-05', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '29880', '33600', 1075745.0, '2021-12-06', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '05236', '66758', 327638.0, '2021-03-20', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '33769', '08158', 9326626.0, '2022-08-08', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '83886', '38751', 2668269.0, '2021-11-22', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '54122', '44116', 5544075.0, '2021-10-27', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '08158', '36132', 2272671.0, '2021-11-01', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '34759', '42581', 1428654.0, '2021-03-07', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '06234', '05236', 506272.0, '2021-08-03', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '22718', '35517', 1309261.0, '2022-10-29', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '40732', '06234', 579258.0, '2021-05-13', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '42581', '75317', 4603102.0, '2022-06-13', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '65919', '75317', 4679527.0, '2021-12-23', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '78956', '40732', 2531143.0, '2021-04-01', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '73733', '63934', 7893424.0, '2022-11-09', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '90625', '26902', 7719256.0, '2021-04-06', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '12182', '54122', 3452933.0, '2022-10-03', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '74338', '33769', 7245873.0, '2021-08-28', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '75825', '66758', 8844072.0, '2022-01-26', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '41403', '14655', 4225779.0, '2022-02-05', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '37637', '38800', 1687793.0, '2022-02-21', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '85721', '52309', 1562841.0, '2021-10-06', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '38656', '70482', 1743395.0, '2021-07-12', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '75284', '65232', 5634107.0, '2021-09-23', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '24763', '65053', 973176.0, '2022-05-15', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '12570', '90625', 1612478.0, '2021-04-20', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '77822', '04766', 6339934.0, '2022-11-22', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '83776', '14655', 5279722.0, '2021-03-24', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '50616', '44116', 286384.0, '2021-09-06', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '07426', '14296', 5566993.0, '2021-11-08', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '32255', '14296', 5974177.0, '2021-07-16', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '72944', '75317', 4883570.0, '2021-09-11', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '62725', '75317', 9052949.0, '2021-11-05', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '15348', '05236', 409664.0, '2021-11-23', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '56486', '75032', 4605774.0, '2021-04-12', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '18374', '38800', 3945678.0, '2021-06-26', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '27239', '75032', 8001973.0, '2022-06-25', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '47168', '38800', 3294284.0, '2021-02-25', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '26335', '70482', 6613050.0, '2021-12-10', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '67261', '12182', 7050520.0, '2021-05-13', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '11135', '36132', 5314898.0, '2021-07-08', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '62126', '28363', 9658.0, '2021-11-28', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '34266', '04766', 9496440.0, '2021-11-10', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '21666', '93242', 3601142.0, '2021-08-01', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '19798', '40732', 6296964.0, '2021-09-29', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '65364', '04766', 1779850.0, '2022-01-02', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '17767', '35229', 4915811.0, '2021-12-03', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '64428', '77451', 9099537.0, '2022-01-09', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '33247', '90625', 2824617.0, '2022-01-29', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '15817', '08341', 7402977.0, '2021-12-31', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '76073', '63934', 2698118.0, '2022-02-03', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '88336', '65919', 6474705.0, '2021-09-07', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '46426', '93242', 8134702.0, '2022-05-24', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '83027', '26902', 5319487.0, '2021-08-27', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '43036', '14655', 368264.0, '2021-09-18', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '26620', '77020', 6171871.0, '2022-08-31', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '27526', '47952', 1736761.0, '2021-08-09', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '28686', '76877', 1084809.0, '2021-02-20', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '47371', '08341', 5842876.0, '2022-11-05', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '28435', '47952', 3888839.0, '2022-10-10', '2' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '29341', '74338', 8659198.0, '2022-08-27', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '26215', '76877', 5264171.0, '2022-06-14', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '29756', '77020', 8991801.0, '2022-10-22', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '58764', '74338', 8839310.0, '2022-02-11', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '44420', '12182', 3075331.0, '2022-11-03', '1' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '85170', '04766', 3740527.0, '2021-06-17', '3' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '42952', '14655', 4139706.0, '2021-04-04', '4' ); 
INSERT INTO maintenance( id, avion_id, cost, maintenance_date, typemaintenance ) VALUES ( '24158', '38751', 7392114.0, '2021-02-16', '4' ); 
INSERT INTO assuranceavion( id, idavion, idassurance, quantite, dateenregistrement, dateexpiration ) VALUES ( '1', '12182', '1', 1, '2022-12-09', '2023-01-10' ); 
INSERT INTO assuranceavion( id, idavion, idassurance, quantite, dateenregistrement, dateexpiration ) VALUES ( '2', '06234', '2', 1, '2022-12-09', '2023-03-10' ); 
INSERT INTO assuranceavion( id, idavion, idassurance, quantite, dateenregistrement, dateexpiration ) VALUES ( '3', '22718', '2', 1, '2022-12-09', '2023-12-10' ); 
