import { IonButton, IonCheckbox, IonInput, IonItem, IonLabel, useIonAlert } from '@ionic/react'
import React, { useRef } from 'react'
import { useHistory } from 'react-router';
import '../assets/myappcss/Form.css';
type Props = {}

const Form = (props: Props) => {
    const history = useHistory();
    var emailRef = useRef<HTMLIonInputElement>(null);
    var passwordRef = useRef<HTMLIonInputElement>(null);
    const [presentAlert] = useIonAlert();
    function getUserData() {
        return [{ userid: 1, username: 'ranja', password: 'ranja123' }, { userid: 2, username: 'ranja2', password: 'ranja1234' }, { userid: 3, username: 'ranja3', password: 'ranja12345' }];
    }
    function authentificate() {
        var email = emailRef.current?.value;
        var password = passwordRef.current?.value;
        var content= {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({email:email,password:password})
        }
        fetch('http://localhost:8080/administrator/login', content).then(response=>
            response.json()
        ).then((data)=>{
            console.log(data);
            saveToken(data.data.token_value,'user_token')
            ;
            history.push('/list_avion');
        }).catch((error)=>{presentAlert({
            header: 'Sign in failed',
            message: 'Email or password not valid',
            buttons: ['OK'],
        })})
    }
    function saveToken(token:string, token_name:string) {
        localStorage.setItem(token_name, token);
    }
    return (
        <form className="ion-padding myform">
            <IonLabel className='formtitle'>Login Form</IonLabel>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Email</IonLabel>
                <IonInput ref={emailRef} />
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Password</IonLabel>
                <IonInput ref={passwordRef} type="password" />
            </IonItem>
            <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={authentificate}>
                Login
            </IonButton>
        </form>
    )
}

export default Form