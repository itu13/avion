import { IonMenu, IonHeader, IonToolbar, IonTitle, IonContent, IonItem, IonLabel, IonMenuToggle, IonList } from '@ionic/react'
import React from 'react'

type Props = {}

const Menu = (props: Props) => {
    return (
        <IonMenu contentId="main-content">
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Menu Content({sessionStorage.getItem('user') != null ? "@user: " + JSON.parse(sessionStorage.getItem('user') as string).username : ""})</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonList>
                    <IonItem className=""  routerLink={'/login'}>
                        <IonLabel>Log In</IonLabel>
                    </IonItem>
                </IonList>
                <IonList>
                    <IonItem className="" routerLink={'/assurance_avion'}>
                        <IonLabel>Assurance</IonLabel>
                    </IonItem>
                </IonList>
                <IonList>
                    <IonItem className="" routerLink={'/list_avion'}>
                        <IonLabel>Liste avions</IonLabel>
                    </IonItem>
                </IonList>
            </IonContent>

        </IonMenu>
    )
}

export default Menu