import 
{ 
    IonCol,
    IonGrid, 
    IonRow,
    IonContent, 
    IonHeader,
    IonTitle, 
    IonToolbar,
    IonLabel,
    IonSelect,
    IonSelectOption,
} 

from "@ionic/react";
import React, { useState, useEffect } from 'react';
import '../assets/myappcss/Detail_avion.css'
import { url } from "../service/Service";


type Props = {}

const Assurance_expiree: any = (props: Props) => {


        
          //sendRequest('GET', 'http://localhost:8080/avion/05236', )
          //let avion:any =null;
          
     
          //console.log(param);
          let [avion, setavion] = useState<any>(null)
          let [month, setMonth] = useState<any>(12)

          useEffect(()=>{ sendRequest(month)}, [1]);
          

          const changeMonth = (event:any) => {
            setMonth(event.target.value);
            console.log("mois courrant"+event.target.value);
            var current_month = event.target.value;

            sendRequest(current_month);

          }
               async function sendRequest(current_month:number) {
                    const token = localStorage.getItem('user_token')?localStorage.getItem('user_token'):"amVhbkBnbWFpbC5jb21qZWFuMTIzNDE2NzA4NjQyNzM2OTY=";
                    const content = {
                         "method": "get",
                         "headers": {
                              "Authorization": "Bearer "+token
                         }
                    }
                    fetch(url+"avions/assurance/"+current_month, content)
                         .then(res => res.json())
                         .then(
                              (result) => {
                                   avion = result.data;
                                   console.log("mois traité"+month);
                                   setavion(result.data);
                                   console.log(avion)
                              },
                              (error) => {
                                   //alert(error);
                                   console.log(error);
                              }
                         );
               }
            
 
if (avion != null)
  {          
    return(
        <IonContent fullscreen>
            <IonHeader>
              <IonToolbar>
                <IonTitle>Liste des vehicules</IonTitle>
              </IonToolbar>
            </IonHeader>
           
            <IonLabel position="floating">Expire dans moins de</IonLabel>
             <IonSelect name="" id="" value={month} onIonChange={changeMonth} className="select-custom">
                <IonSelectOption value="1">1 mois</IonSelectOption>
                <IonSelectOption value="3">3 mois</IonSelectOption>
             </IonSelect>
              <IonGrid className="table">
              <IonRow>
                  
                  <IonCol className="col-title">Durée restante assurance</IonCol>
                  <IonCol className="col-title">Plane number</IonCol>
              </IonRow>
              
             {avion.map((item: any, index: number) => 
              
                   <IonRow>
                      <IonCol className="col-info">{item.monthdiff}</IonCol>
                      <IonCol className="col-info"> <a href={`/detail_avion/${item.id}`}>{item.planenumber}</a></IonCol>
                    </IonRow>
              
              
                )}
              </IonGrid>
            </IonContent>
          
        );
  }

  else
  {
    return(
      <IonContent>
         <IonLabel position="floating">Expire dans moins de</IonLabel>
        <IonSelect name="" id="" value={month} onIonChange={changeMonth} className="select-custom" placeholder="Choisir expiration">
                <IonSelectOption value="1">1 mois</IonSelectOption>
                <IonSelectOption value="3">3 mois</IonSelectOption>
             </IonSelect>
        </IonContent>
    )
  }
};

export default Assurance_expiree;
