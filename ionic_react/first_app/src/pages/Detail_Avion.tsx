import { IonBackButton, IonButton, IonCol, IonContent, IonGrid, IonLabel, IonRow, useIonViewWillEnter } from '@ionic/react'
import React, { useEffect, useState } from 'react'
import Form from '../components/Form'

import '../assets/myappcss/Detail_avion.css'
import { useParams } from 'react-router'
import { Camera, CameraResultType, Photo, CameraSource } from "@capacitor/camera"
import { getContent, getToken, url } from '../service/Service'

type Props = {}
const Detail_avion = (props: Props) => {

     let param = JSON.stringify(useParams());
     let id = JSON.parse(param).id;

     function updateImage(id: any, extension: any, base64: any) {
          const content =
          {
               "method": 'put',
               "headers": {
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + getToken()
               },
               "body": JSON.stringify({ "base64": base64, "format": extension })
          }
          fetch(url + 'avion/' + id + '/photo', content).then(response =>
               response.json()
          ).then((data) => {
               setavion(data.data);
               const pic = {
                    "base64String": data.data.base64Image,
                    "format": data.data.formatImage,
               }
               setPhoto(pic);
               alert(data.data);
               //console.log("resultat photo"+data.data);
               alert("changer");
               ////console.log(data);          
          }, (error) => {
               alert(error);
               //console.log("error"+error);
          })
          /*//console.log("data lasa"+JSON.stringify([{"base64":base64},{"format":extension}]));
          //console.log(base64)
          //console.log(extension)*/
     }
     let [avion, setavion] = useState<any>(null)
     let [photo, setPhoto] = useState<any>("")

     useEffect(() => { sendRequest() }, []);

     async function sendRequest() {
          const token = localStorage.getItem('user_token') ? localStorage.getItem('user_token') : "";
          const content = {
               "method": "get",
               "headers": {
                    "Authorization": "Bearer " + token
               }
          }
          // ic il y a un url
          fetch(url + "avion/" + id, content)
               .then(res => res.json())
               .then(
                    (result) => {
                         avion = result;
                         //console.log(avion);
                         setavion(result.data)
                         const pic = {
                              "base64String": result.data.base64Image,
                              "format": result.data.formatImage,
                         }
                         setPhoto(pic)
                    },
                    (error) => {
                         //alert(error);
                         //console.log(error);
                    }
               );
     }

     const TakePicture = async () => {

          const pic = await Camera.getPhoto({
               quality: 90,
               allowEditing: true,
               resultType: CameraResultType.Base64,
               source: CameraSource.Photos

          })
          /*//console.log("format"+photo.format);
          //console.log("base64"+photo.base64String);*/
          updateImage(avion.id, pic.format, pic.base64String);
          //sendRequest();
          /*//console.log(photo);*/
     }

     const ChoosePicture = async () => {
          const pic = await Camera.getPhoto({
               quality: 90,
               allowEditing: true,
               resultType: CameraResultType.Base64,
               source: CameraSource.Photos

          })
          //setPhoto()
          /*//console.log("format"+photo.format);
          //console.log("base64"+photo.base64String);*/
          updateImage(avion.id, pic.format, pic.base64String);
          //sendRequest();
          ////console.log(photo);        
     }

     function GetImage(){
          if(photo!=null && photo.format!=null && photo.base64String!=null){
               return (<p><img src={`data:assets/Images/${photo.format};base64,${photo.base64String}`} width="150" /></p>)
          }else{
               return (<p>
                Pas encore d'image
               </p>)
          }
     }

     if (avion != null) {
          return (
               <IonContent className="ion-padding">
                    <h1>Numero plane: {avion.planenumber} </h1>
                    {GetImage()}
                    <IonButton onClick={ChoosePicture}>galerie</IonButton>
                    <IonButton onClick={TakePicture}>camera</IonButton>
                    <li>Marque: {avion.brand.brandname}</li>
                    <li> Date d'achat: {avion.creationdate} </li>
                    <p>Kilometrages:  </p>
                    <IonGrid className="table">
                         <IonRow>
                              <IonCol className="col-title">Date</IonCol>
                              <IonCol className="col-title">Debut km</IonCol>
                              <IonCol className="col-title">Fin km</IonCol>
                         </IonRow>
                         {avion['kilometrages'].map((kilom: any, index: number) =>
                              <IonRow>
                                   <IonCol className="col-info">{kilom.checkin_date}</IonCol>
                                   <IonCol className="col-info">{kilom.starting_km.toLocaleString(undefined, { maximumFractionDigits: 2 })}</IonCol>
                                   <IonCol className="col-info">{kilom.ending_km.toLocaleString(undefined, { maximumFractionDigits: 2 })}</IonCol>
                              </IonRow>
                         )}

                    </IonGrid>

               </IonContent>
          )
     } else {
          return (
               <IonContent className="ion-padding">
                    <h1>Aucun avion correspondant</h1>
               </IonContent>
          )
     }

}

export default Detail_avion