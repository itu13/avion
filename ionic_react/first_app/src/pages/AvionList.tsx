import 
{ 
    IonCol,
    IonGrid, 
    IonRow,
    IonContent, 
    IonHeader,
    IonTitle, 
    IonToolbar,
} 

from "@ionic/react";
import React, { useState, useEffect } from 'react';
import '../assets/myappcss/Detail_avion.css'
import { url } from "../service/Service";


type Props = {}

const AvionList: any = (props: Props) => {
          let [avion, setavion] = useState<any>(null)
          useEffect(()=>{
               async function sendRequest() {
                    //const token = localStorage.getItem('user_token')?localStorage.getItem('user_token'):"amVhbkBnbWFpbC5jb21qZWFuMTIzNDE2NzA4NjQyNzM2OTY=";
                    const content = {
                         "method": "get"
                         
                    }
                    //ic il y a un url
                    fetch(url+"avions", content)
                         .then(res => res.json())
                         .then(
                              (result) => {
                                   avion = result.data;
                                   console.log(avion);
                                   setavion(result.data)
                              },
                              (error) => {
                                   console.log(error);
                              }
                         );
               }
               sendRequest();
          },[1]);
 
if (avion != null)
  {          
    return(
        <IonContent fullscreen>
            <IonHeader>
              <IonToolbar>
                <IonTitle>Liste des Avions</IonTitle>
              </IonToolbar>
            </IonHeader>
           
             
              <IonGrid className="table">
              <IonRow>
                  <IonCol className="col-title">Brand</IonCol>
                  <IonCol className="col-title">Planenumber</IonCol>
              </IonRow>
             {avion.map((item: any, index: number) => 
              
                   <IonRow>
                      <IonCol className="col-info">{item.brand.brandname}</IonCol>
                      
                      {/* ici il y a un url*/}
                      <IonCol className="col-info"> <a href={`/detail_avion/${item.id}`}>{item.planenumber}</a></IonCol>
                    </IonRow>
              
              
                )}
              </IonGrid>
            </IonContent>
          
        );
  }

  else
  {
    return(
      <IonContent>
        <h1>erreur de chargement des vehicules</h1>
        </IonContent>
    )
  }
};

export default AvionList;
