export const url = "http://localhost:8080/"

export const getContent = (methode: string, body: string, istoken: boolean = false) => {
    const token =localStorage.getItem('user_token') ? localStorage.getItem('user_token') : "";
    if (istoken == true) {
        return {
            "method": methode,
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + token
            },
            "body": body
        }
    } else {
        return {
            "method": methode,
            "headers": {
                'Content-Type': 'application/json'
            },
            "body": body
        }
    }
}
export const getToken=()=>{
    return localStorage.getItem('user_token') ? localStorage.getItem('user_token') : "";
}