import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonButtons, IonContent, IonHeader, IonMenu, IonMenuButton, IonPage, IonRouterOutlet, IonTitle, IonToolbar, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Login from './pages/Login';
import Header from './components/Header';
import Footer from './components/Footer';
import './assets/myappcss/App.css';
import Home from './pages/Home';
import Menu from './components/Menu';
import MenuHeader from './components/MenuHeader';
import InfiniteScrollExample from './components/InfiniteScrollExample';
import Assurance_expiree from './pages/Assurance_expiree';
import AvionList from './pages/AvionList';
import Detail_avion from './pages/Detail_Avion';
setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route path="">
          <Menu />
          <IonPage id="main-content">
            <Header />
            <Login />
          </IonPage>
        </Route>
        <Route path="/login">
          <Header />
          <Login />
        </Route>
        <Route path="/home">
          <Menu />
          <IonPage id="main-content">
            <MenuHeader />
            <Home />
            <Footer />
          </IonPage>
        </Route>
        <Route path="/scroll">
          <InfiniteScrollExample />
        </Route>

        {/* ici il y a un url */}
        <Route path="/list_avion">
          <Menu />
          <IonPage id="main-content">
            <MenuHeader />
            <AvionList />
            <Footer />
          </IonPage>

        </Route>


        {/* ici il y a un url */}
        <Route path="/detail_avion/:id">
          <Menu />
          <IonPage id="main-content">
            <MenuHeader />
            <Detail_avion />
            <Footer />
          </IonPage>
        </Route>

        {/* ici il y a un url */}
        <Route path="/assurance_avion">
          <Menu />
          <IonPage id="main-content">
            <MenuHeader />
            <Assurance_expiree />
            <Footer />
          </IonPage>

        </Route>



      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
