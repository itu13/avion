package sb.avion.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sb.avion.models.Avion;
import sb.avion.models.VAssurance;
import sb.avion.repository.AvionRepository;
import sb.avion.repository.VAssuranceRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class AvionService {
    @Autowired
    private AvionRepository AvionRepository;

    @Autowired
    private VAssuranceRepository assuranceRepos;

    @Autowired
    private PhotoService photoService;
    
    // get sequence value
    public String getSequenceValue() {
        return AvionRepository.getSeqValue().toString();
    }

    public Avion saveAvion(Avion Avion) {
        return AvionRepository.save(Avion);
    }

    public Optional<Avion> findById(String id) {
        return AvionRepository.findById(id);
    }

    public Iterable<Avion> findAllAvions() {
        return AvionRepository.findAll();
    }

    public void deleteAvion(Avion Avion) {
        AvionRepository.delete(Avion);
    }

    public List<VAssurance> assurance_bymonth(Integer month) {
        return assuranceRepos.liste_assurance(month);
    }
    public List<VAssurance>allassurance(){
        return assuranceRepos.findAll();
    }

    public Avion updatePhoto(HashMap<String, String>data, String id)throws Exception{
        Optional<Avion> temp = findById(id);
        if (temp.isPresent()){
            Avion exist = temp.get();
            boolean bool=photoService.authenPhoto(data.get("base64"), data.get("format"));
            if(bool){
                exist.setPhoto(data.get("base64")+"_"+ data.get("format"));
            }else{
                throw new Exception("format et image non conformes");
            }
            return exist;         
            //System.out.println("photo"+avion.getPhoto());            
        }else{
            throw new Exception("avion inexistant");
        }

    }
    
}
