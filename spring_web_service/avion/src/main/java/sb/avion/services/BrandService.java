package sb.avion.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sb.avion.models.Brand;
import sb.avion.repository.BrandRepository;

@Service
public class BrandService {
    @Autowired
    private BrandRepository brandRepository;
    // method that find a brand by id
    public Brand findBrandById(String id) {
        return brandRepository.findById(id).get();
    }

}
