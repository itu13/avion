package sb.avion.services;

import org.springframework.stereotype.Service;

@Service
public class PhotoService {
     private final static String[] liste_format=new String[]{"png", "jpg", "jpeg"};
     public boolean authenPhoto(String base64, String format)throws Exception{ //verifie si la photo est correct
          if(base64==null || base64.equals("")){
              throw new Exception("pas d'image");
          }
          if(format==null || format.equals("")) throw new Exception("format non pris en charge");
          for(String s:liste_format){
              if(format.equals(s)){
                  return true;
              }
          }
          throw new Exception("format non pris en charge");
      }
}
