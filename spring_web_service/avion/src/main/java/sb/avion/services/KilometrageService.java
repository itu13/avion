package sb.avion.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sb.avion.models.Kilometrage;
import sb.avion.repository.KilometrageRepository;

import java.util.List;

@Service
public class KilometrageService {
    @Autowired
    private KilometrageRepository kilometrageRepository;
    // get sequence value
    public String getSequenceValue() {
        return kilometrageRepository.getSeqValue().toString();
    }

    public Kilometrage saveKilometrage(Kilometrage kilometrage) {
        return kilometrageRepository.save(kilometrage);
    }

    public Kilometrage findById(String id) {
        return kilometrageRepository.findById(id).get();
    }

    public List<Kilometrage> findAllKilometrages() {
        return kilometrageRepository.findAll();
    }

    public void deleteKilometrage(Kilometrage km) {
        kilometrageRepository.delete(km);
    }
}
