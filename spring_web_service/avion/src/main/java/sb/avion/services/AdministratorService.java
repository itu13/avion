package sb.avion.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sb.avion.models.Administrator;
import sb.avion.repository.AdministratorRepository;

@Service
public class AdministratorService {
    // dependency injection of AdministratorRepository
    @Autowired//asiana instance ao anatiny rehefa execute
    private AdministratorRepository administratorRepository;
    @Autowired
    private TokenService tokenService;
    // constructor with dependency injection
    public AdministratorService(AdministratorRepository administratorRepository) {
        this.administratorRepository = administratorRepository;
    }

    // methods that saves an administrator
    public Administrator saveAdministrator(Administrator administrator) {
        administratorRepository.save(administrator);
        return administrator;
    }

    public String getSequenceValue() {
        return administratorRepository.getSeqValue().toString();
    }

    // method that finds by email and password
    public Administrator findByEmailAndPassword(String email, String password) {
        return administratorRepository.findByEmailAndPassword(email, password);
    }

    // method that finds by id
    public Administrator findById(String id) {
        return administratorRepository.findById(id).get();
    }

    public void logOut(String tokenvalue){
        this.tokenService.deleteToken(tokenvalue);
    }
}
