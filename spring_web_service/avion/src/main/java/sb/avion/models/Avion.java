package sb.avion.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Date;
import java.util.List;

@Entity(name="Avion")
public class Avion {
    @Id
    private String id;
    @JsonIgnore
    private String photo;
    private String planenumber;
    private Date creationdate;
    @ManyToOne
    private Administrator administrator;
    @ManyToOne
    private Brand brand;

    @OneToMany(mappedBy = "avion")
    private List<Kilometrage> kilometrages;

    
    //empty constructor
    public Avion() {
    }

    //constuctor with parameters
    public Avion(String id, String planenumber, Date creationdate, Administrator administrator, Brand brand) {
        this.id = id;
        this.planenumber = planenumber;
        this.creationdate = creationdate;
        this.administrator = administrator;
        this.brand = brand;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Date creationdate) {
        this.creationdate = creationdate;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public List<Kilometrage> getKilometrages() {
        return kilometrages;
    }

    public void setKilometrages(List<Kilometrage> kilometrages) {
        this.kilometrages = kilometrages;
    }

    public String getPlanenumber() {
        return planenumber;
    }

    public void setPlanenumber(String planenumber) {
        this.planenumber = planenumber;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    public String getBase64Image(){
        if(this.photo!=null){
            return this.photo.split("_")[0];
        }
        return null;
    }
    public String getFormatImage(){
        if(this.photo!=null){
            return this.photo.split("_")[1];
        }
        return null;
    }
}
