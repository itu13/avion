package sb.avion.models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

@Entity(name = "brand")
public class Brand {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

    @ManyToOne
    private Administrator administrator;
    private String brandname;

    @JsonIgnore
    @OneToMany(mappedBy = "brand")
    List<Avion> avions;

    public Brand(String id, Administrator admin, String brandname) {
        this.id = id;
        this.administrator = admin;
        this.brandname = brandname;
    }

    public Brand() {

    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public List<Avion> getAvions() {
        return avions;
    }

    public void setAvions(List<Avion> avions) {
        this.avions = avions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Administrator getAdmin() {
        return administrator;
    }

    public void setAdmin(Administrator admin) {
        this.administrator = admin;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }
}
