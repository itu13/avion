package sb.avion.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Date;

@Entity(name="kilometrage")
public class Kilometrage {
    @Id
    private String id;
    @ManyToOne
    private Administrator administrator;
    @JsonIgnore//ignore cet attribut quand ca transforme en json
    @ManyToOne
    private Avion avion;
    private double starting_km;
    private double ending_km;
    private Date checkin_date;

    public Kilometrage() {
    }

    public Kilometrage(String id, Administrator administrator, Avion avion, double starting_km, double ending_km, Date checkin_date) {
        this.id = id;
        this.administrator = administrator;
        this.avion = avion;
        this.starting_km = starting_km;
        this.ending_km = ending_km;
        this.checkin_date = checkin_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public Avion getAvion() {
        return avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public double getStarting_km() {
        return starting_km;
    }

    public void setStarting_km(double starting_km) {
        this.starting_km = starting_km;
    }

    public double getEnding_km() {
        return ending_km;
    }

    public void setEnding_km(double ending_km) {
        this.ending_km = ending_km;
    }

    public Date getCheckin_date() {
        return checkin_date;
    }

    public void setCheckin_date(Date checkin_date) {
        this.checkin_date = checkin_date;
    }
}
