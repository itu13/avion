package sb.avion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity(name = "administrator")
public class Administrator {
    @Id
    private String id;
    private String firstname;
    private String lastname;
    private String email;

    private String password;
    private Date birthdate;
    private Date creationdate;

    @OneToMany(mappedBy = "administrator")
    private List<Avion> Avions;

    @OneToMany(mappedBy = "administrator")
    private List<Brand> brands;

    @OneToMany(mappedBy = "administrator")
    private List<Kilometrage> kilometrages;
    public Administrator() {
    }

    @OneToMany(mappedBy = "administrator")
    private List<Token> tokens;

    public Administrator(String id, String firstname, String lastname, String email, String password, Date birthdate, Date creationdate) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.birthdate = birthdate;
        this.creationdate = creationdate;
    }

    @JsonIgnore
    public List<Avion> getAvions() {
        return Avions;
    }

    public void setAvions(List<Avion> Avions) {
        this.Avions = Avions;
    }
    @JsonIgnore
    public List<Brand> getBrands() {
        return brands;
    }
    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }
    @JsonIgnore
    public List<Kilometrage> getKilometrages() {
        return kilometrages;
    }

    public void setKilometrages(List<Kilometrage> kilometrages) {
        this.kilometrages = kilometrages;
    }

    @JsonIgnore
    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

    public void removeToken(Token token) {
        this.tokens.remove(token);
    }
    public void addToken(Token token) {
        this.tokens.add(token);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Date getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Date creationdate) {
        this.creationdate = creationdate;
    }
}
