package sb.avion.models;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
@Entity
@Table(name="v_assurance")
@Data
public class VAssurance {
     @Id
     private String id;

     @JsonIgnore
     @ManyToOne
     @JoinColumn(name="brand_id")
     private Brand brand;

     private String planenumber;

     private Date creationdate;

     @JsonIgnore
     @ManyToOne
     @JoinColumn(name="administrator_id")
     private Administrator administrator;
     
     private Integer monthdiff;

}
