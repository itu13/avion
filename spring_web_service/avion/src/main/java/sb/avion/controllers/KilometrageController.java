package sb.avion.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import sb.avion.models.Administrator;
import sb.avion.models.Avion;
import sb.avion.models.Kilometrage;
import sb.avion.models.responses.Error;
import sb.avion.models.responses.Success;
import sb.avion.models.responses.SuccessMessage;
import sb.avion.services.AdministratorService;
import sb.avion.services.AvionService;
import sb.avion.services.KilometrageService;
import sb.avion.services.TokenService;

import javax.servlet.http.HttpServletResponse;

@RestController
public class KilometrageController {
    @Autowired
    private KilometrageService kilometrageService;
    @Autowired
    private AvionService AvionService;

    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private TokenService tokenService;
    // function that saves a new kilometrage of Avion
    @PostMapping("administrator/{idadministrator}/Avion/{idAvion}/kilometrage")
    public ResponseEntity<Object> saveKilometrage(@PathVariable String idadministrator, @PathVariable String idAvion, @RequestBody Kilometrage kilometrage, @RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Avion Avion = AvionService.findById(idAvion).get();
            kilometrage.setAvion(Avion);
            Administrator admin=administratorService.findById(idadministrator);
            kilometrage.setAdministrator(admin);
            kilometrage.setId(kilometrageService.getSequenceValue());
            Success success=new Success(kilometrageService.saveKilometrage(kilometrage));
            return ResponseEntity.ok().body(success);
        }catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }

    @GetMapping("/kilometrage/{id}")
    public ResponseEntity<Object> findKilometrageById(@PathVariable String id,@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Success success=new Success(kilometrageService.findById(id));
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }
    @GetMapping("/kilometrages")
    public ResponseEntity<Object> findAllKilometrages(@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Success success=new Success(kilometrageService.findAllKilometrages());
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }

    @PutMapping("/administrator/{idadministrator}/Avion/{idAvion}/kilometrage/{idkilometrage}")
    public ResponseEntity<Object> updateKilometrage(@PathVariable String idadministrator,@PathVariable String idAvion,@PathVariable String idkilometrage, @RequestBody Kilometrage updatedKilometrage,@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Avion Avion = AvionService.findById(idAvion).get();
            Administrator admin=administratorService.findById(idadministrator);
            updatedKilometrage.setId(idkilometrage);
            updatedKilometrage.setAvion(Avion);
            updatedKilometrage.setAdministrator(admin);
            Success success=new Success(kilometrageService.saveKilometrage(updatedKilometrage));
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }

    @DeleteMapping("/kilometrage/{id}")
    public ResponseEntity<Object> deleteKilometrage(@PathVariable String id,@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Kilometrage km=kilometrageService.findById(id);
            kilometrageService.deleteKilometrage(km);
            Success success=new Success(km);
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }
}
