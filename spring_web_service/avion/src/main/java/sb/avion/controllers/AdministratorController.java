package sb.avion.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import sb.avion.models.Administrator;
import sb.avion.models.Token;
import sb.avion.models.responses.Error;
import sb.avion.models.responses.Success;
import sb.avion.models.responses.SuccessMessage;
import sb.avion.services.AdministratorService;
import sb.avion.services.TokenService;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.NoSuchElementException;

@RestController
public class AdministratorController {
    @Autowired
    private AdministratorService administratorService;
    @Autowired
    private TokenService tokenService;
    //method that saves an administrator
    @PostMapping("/administrator")
    public ResponseEntity<Object> saveAdministrator(@RequestBody Administrator administrator,@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try{
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            administrator.setId(administratorService.getSequenceValue());
            Administrator adminResp=this.administratorService.saveAdministrator(administrator);
            Success success=new Success(adminResp);
            return ResponseEntity.ok().body(success);
        }catch (Exception e){
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }

    // method that finds by email and password
    @PostMapping("/administrator/login")
    public ResponseEntity<Object> login(@RequestBody Administrator administrator) {
        Administrator foundAdmin=this.administratorService.findByEmailAndPassword(administrator.getEmail(), administrator.getPassword());
        if (foundAdmin==null) {
            Error error= new Error(400,"Admin not found");
            return ResponseEntity.badRequest().body(error);
        }
        // check the administrator already have a token
        Token currentToken=this.tokenService.findTokenByIdadministrator(foundAdmin.getId());
        if (currentToken!=null) {
            currentToken.setAdministrator(null);
            currentToken.setId(null);
            Success success=new Success(currentToken);
            return ResponseEntity.ok().body(success);
        }
        currentToken=new Token();
        String token= TokenService.generateToken(administrator.getEmail(),administrator.getPassword());
        currentToken.setAdministrator(foundAdmin);currentToken.setToken_value(token);currentToken.setCreation_date(new Date());
        currentToken.setExpiration_date(new Date(new Date().getTime()+Token.EXPIRATION));currentToken.setId(tokenService.getSequenceValue());
        tokenService.save(currentToken);
        currentToken.setAdministrator(null);
        currentToken.setId(null);
        Success success=new Success(currentToken);
        return ResponseEntity.ok().body(success);
        //return ResponseEntity.ok().body(foundAdmin);
    }

    // methods that allows user to log out
    @GetMapping("/administrator/{admin_id}/logout")
    public ResponseEntity<Object> logout(@PathVariable String admin_id, @RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        // get the admin by admin_id
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            this.administratorService.findById(admin_id);
            // find token by adminid
            Token currentToken=this.tokenService.findTokenByIdadministrator(admin_id);
            // remove the token if it is found
            if (currentToken!=null) {
                this.tokenService.delete(currentToken);
                SuccessMessage m=new SuccessMessage("Logout successfully");
                Success success=new Success(m);
                return ResponseEntity.ok().body(success);
            }
            Error error=new Error(400,"Token doesn't exist");
            return ResponseEntity.badRequest().body(error);
        }catch (Exception e){
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }else if(e instanceof  NoSuchElementException){
                error= new Error(400,"Admin not found");
            }
            return ResponseEntity.badRequest().body(error);
        }

    }
}
