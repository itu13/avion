package sb.avion.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.function.EntityResponse;

@RestController
public class ErrorController {
    @GetMapping("/notloggedin")
    public ResponseEntity<Object> error() {
        return ResponseEntity.badRequest().body("You are not logged in");
    }
}
