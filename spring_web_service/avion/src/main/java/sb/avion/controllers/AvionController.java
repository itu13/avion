package sb.avion.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import sb.avion.models.Administrator;
import sb.avion.models.Avion;
import sb.avion.models.Brand;
import sb.avion.models.VAssurance;
import sb.avion.models.responses.Error;
import sb.avion.models.responses.Success;
import sb.avion.services.AdministratorService;
import sb.avion.services.AvionService;
import sb.avion.services.BrandService;
import sb.avion.services.TokenService;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
@RestController
public class AvionController {
    @Autowired
    private AvionService avionService;
    @Autowired
    private AdministratorService administratorService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private TokenService tokenService;

    

    //function that saves a new Avion
    @PostMapping("/administrator/{idadmin}/brand/{idbrand}/Avion")
    public ResponseEntity<Object> saveAvion(@PathVariable String idadmin, @PathVariable String idbrand, @RequestBody Avion Avion,@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Administrator admin = administratorService.findById(idadmin);
            Brand brand = brandService.findBrandById(idbrand);
            Avion.setAdministrator(admin);
            Avion.setBrand(brand);
            Avion.setId(avionService.getSequenceValue());
            Success success=new Success(avionService.saveAvion(Avion));
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }

    @GetMapping("/avion/{id}")
    public ResponseEntity<Object> findAvionById(@PathVariable String id,@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Success success=new Success(avionService.findById(id).get());
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }

    @GetMapping("/avions")
    public ResponseEntity<Object> findAllavionS(@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            Success success=new Success(avionService.findAllAvions());
            return ResponseEntity.ok().body(success);
        }catch (Exception e) {
            Error error = new Error(400,e.getMessage());
            return ResponseEntity.badRequest().body(error);
        }
    }

    @PutMapping("/administrator/{idadmin}/brand/{idbrand}/avion/{idavion}")
    public ResponseEntity<Object> updateAvion(@PathVariable String idadmin, @PathVariable String idbrand, @PathVariable String idavion, @RequestBody Avion updatedAvion,@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Administrator admin = administratorService.findById(idadmin);
            Brand brand = brandService.findBrandById(idbrand);
            updatedAvion.setAdministrator(admin);
            updatedAvion.setBrand(brand);
            updatedAvion.setId(idavion);
            Success success=new Success(avionService.saveAvion(updatedAvion));
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }

    @DeleteMapping("/avion/{id}")
    public ResponseEntity<Object> deleteAvion(@PathVariable String id,@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Avion avion = avionService.findById(id).get();
            avionService.deleteAvion(avion);
            Success success = new Success(avion);
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }
            return ResponseEntity.badRequest().body(error);
        }
    }
    @GetMapping("/avions/assurance/{month}")
    public ResponseEntity<Object> findAvionByAssurancebyMonth(@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse, @PathVariable(name="month")Integer month) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            List<VAssurance>liste=avionService.assurance_bymonth(month);
            Success success=new Success(liste);
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            //Error error = new Error(400, e.getMessage()); 
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }          
            return ResponseEntity.badRequest().body(error);
        }
    }
    @GetMapping("/avions/assurance")
    public ResponseEntity<Object> findAvionByAssurance(@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse) {
        try {
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            List<VAssurance>liste=avionService.allassurance();
            Success success=new Success(liste);
            return ResponseEntity.ok().body(success);
        } catch (Exception e) {
            //Error error = new Error(400, e.getMessage());     
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }      
            return ResponseEntity.badRequest().body(error);
        }
    }

    @PutMapping("/avion/{idavion}/photo")
    public ResponseEntity<Object> changePhoto(@RequestHeader HttpHeaders headers, HttpServletResponse httpResponse,@PathVariable String idavion, @RequestBody HashMap<String, String>data){
        try{
            if(tokenService.hasToken(headers,httpResponse)==null){
                Error error=new Error(400,"You are not logged in");
                return ResponseEntity.badRequest().body(error);
            }
            Avion exist=null;
            try{
                exist=avionService.updatePhoto(data, idavion);
                Success success=new Success(avionService.saveAvion(exist));
                return ResponseEntity.ok().body(success);
            }catch(Exception e){
                Error error=new Error(400,e.getMessage());
                return ResponseEntity.badRequest().body(error);
            }            
        }
        catch (Exception e){
            //e.printStackTrace();
            Error error = new Error(400, "No Bearer Token found in the header");
            if(e instanceof NullPointerException){
                error = new Error(400, "No Bearer Token found in the header");
            }  
            return ResponseEntity.badRequest().body(error);
        }
    }   
}
