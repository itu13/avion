package sb.avion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sb.avion.models.Administrator;

public interface AdministratorRepository extends JpaRepository<Administrator, String> {
    @Query(value = "select nextval('seqadministrator')", nativeQuery =
            true)
    Long getSeqValue();

    // method that finds by email and password
    @Query(value = "select * from administrator where email = ?1 and password = ?2 limit 1", nativeQuery = true)
    Administrator findByEmailAndPassword(String email, String password);

}
