package sb.avion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sb.avion.models.Avion;

public interface AvionRepository extends JpaRepository<Avion, String> {
    @Query(value = "select nextval('seqAvion')", nativeQuery =
            true)
    Long getSeqValue();
}
