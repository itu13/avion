package sb.avion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sb.avion.models.VAssurance;

public interface VAssuranceRepository extends JpaRepository<VAssurance, String> {

    @Query(value="select * from v_assurance where monthdiff < ?1 and monthdiff>0", nativeQuery = true)
    public List<VAssurance>liste_assurance(Integer month);
}
