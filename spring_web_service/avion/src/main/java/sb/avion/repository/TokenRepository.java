package sb.avion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sb.avion.models.Token;

public interface TokenRepository extends JpaRepository <Token,String> {
    @Query(value = "select nextval('seqtoken')", nativeQuery =
            true)
    Long getSeqValue();

    // method that find a token by idadministrator
    @Query(value = "select * from v_validtoken where administrator_id = ?1 limit 1", nativeQuery = true)
    Token findByIdadministrator(String idadministrator);

    @Query(value = "select * from v_validtoken where token_value = ?1 limit 1", nativeQuery = true)
    Token findByTokenValue(String tokenValue);

    @Query(value = "delete from token where token_value= ?1", nativeQuery = true)
    void deleteTokenByValue(String token_value);
}
