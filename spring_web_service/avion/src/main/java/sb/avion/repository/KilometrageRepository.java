package sb.avion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sb.avion.models.Kilometrage;

public interface KilometrageRepository extends JpaRepository<Kilometrage, String> {
    @Query(value = "select nextval('seqkilometrage')", nativeQuery =
            true)
    Long getSeqValue();
}
