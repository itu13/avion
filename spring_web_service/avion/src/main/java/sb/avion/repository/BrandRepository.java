package sb.avion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sb.avion.models.Brand;

public interface BrandRepository extends JpaRepository<Brand, String> {
}
